with Bluetooth_Low_Energy;
with Bluetooth_Low_Energy.Beacon;
with Bluetooth_Low_Energy.Packets;
with NRF51.Clock;
with NRF51.Events;
with nRF51.Tasks;
with nRF51.Radio;
with HAL; use HAL;
use Bluetooth_Low_Energy;
package body Beacon is

  Beacon_Packet : Bluetooth_Low_Energy.Packets.BLE_Packet;
  Current_Adv_Channel : Bluetooth_Low_Energy.BLE_Advertising_Channel_Number := 37;

  ----------------
  -- Initialize --
  ----------------

  procedure Initialize is
    Beacon_UUID : constant Bluetooth_Low_Energy.BLE_UUID :=
      Bluetooth_Low_Energy.Make_UUID ((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16));
  begin
    Beacon_Packet := Bluetooth_Low_Energy.Beacon.Make_Beacon_Packet
      (MAC   => (16#A1#, 16#AD#, 16#A1#, 16#AD#,16#A1#, 16#AD#),
       UUID  => Beacon_UUID,
       Major => 0,
       Minor => 0,
       Power => 0);

    NRF51.Clock.Set_High_Freq_Source (Src => NRF51.Clock.HFCLK_RC);
    NRF51.Clock.Start_High_Freq;
    while not NRF51.Clock.High_Freq_Running loop
      null;
    end loop;

    NRF51.Radio.Setup_For_Bluetooth_Low_Energy;

    NRF51.Radio.Set_Logic_Addresses (Base0               => 16#89_BE_D6_00#,
                                     Base1               => 16#00_00_00_00#,
                                     Base_Length_In_Byte => 3,
                                     AP0                 => 16#8E#,
                                     AP1                 => 16#00#,
                                     AP2                 => 16#00#,
                                     AP3                 => 16#00#,
                                     AP4                 => 16#00#,
                                     AP5                 => 16#00#,
                                     AP6                 => 16#00#,
                                     AP7                 => 16#00#);

    NRF51.Radio.Set_TX_Address (0);

    NRF51.Radio.Set_Power (NRF51.Radio.Zero_Dbm);

    NRF51.Radio.Enable_Shortcut (NRF51.Radio.Ready_To_Start);
    NRF51.Radio.Enable_Shortcut (NRF51.Radio.End_To_Disable);

  end Initialize;

  -----------------
  -- Send_Packet --
  -----------------

  procedure Send_Packet is
  begin
    NRF51.Radio.Configure_Whitening
      (True,
       UInt6 (Current_Adv_Channel));

    nRF51.Radio.Set_Frequency
      (nRF51.Radio.Radio_Frequency_MHz (Bluetooth_Low_Energy.Channel_Frequency (Current_Adv_Channel)));

    if Current_Adv_Channel /= Bluetooth_Low_Energy.BLE_Advertising_Channel_Number'Last then
      Current_Adv_Channel := Current_Adv_Channel + 1;
    else
      Current_Adv_Channel := BLE_Advertising_Channel_Number'First;
    end if;

    nRF51.Radio.Set_Packet (Bluetooth_Low_Energy.Packets.Memory_Address (Beacon_Packet));

    nRF51.Events.Clear (Nrf51.Events.Radio_DISABLED);
    NRF51.Events.Clear (NRF51.Events.Radio_ADDRESS);
    NRF51.Events.Clear (NRF51.Events.Radio_PAYLOAD);
    NRF51.Events.Clear (NRF51.Events.Radio_END);

    NRF51.Tasks.Trigger (NRF51.Tasks.Radio_TXEN);

    while not Nrf51.Events.Triggered (NRF51.Events.Radio_DISABLED) loop
      null;
    end loop;

  end Send_Packet;

end Beacon;
