with MicroBit;
with NRF51.GPIO;
with Time;
with nRF51.GPIO;

package body Buttons is

  type Button_State_Array is array (Button_Type) of State_Type;
  Points      : constant array (Button_Type) of nRF51.GPIO.GPIO_Point :=
    (MicroBit.MB_P5, MicroBit.MB_P11);
  States      : Button_State_Array := (others => Released);
  Subscribers : array (1 .. 10) of Callback_Type :=
    (others => null);

  procedure Initialize;
  procedure Handler;

  ----------------
  -- Initialize --
  ----------------

  procedure Initialize is
    Conf : NRF51.GPIO.GPIO_Configuration;
  begin
    Conf.Mode         := NRF51.GPIO.Mode_In;
    Conf.Resistors    := NRF51.GPIO.No_Pull;
    Conf.Input_Buffer := NRF51.GPIO.Input_Buffer_Connect;
    Conf.Sense        := NRF51.GPIO.Sense_Disabled;

    for Pt of Points loop
      Pt.Configure_IO (Conf);
    end loop;

    if not Time.Subscribe (Handler'Access) then
      raise Program_Error;
      -- TODO: Print error
    end if;

  end Initialize;

  -------------
  -- Handler --
  -------------

  procedure Handler is
    Prev_States : constant Button_State_Array := States;
  begin
    for Button in Button_Type loop
      if not nRF51.GPIO.Set (Points (Button)) then
        States (Button) := Pressed;
      else
        States (Button) := Released;
      end if;
    end loop;

    for Button in Button_Type loop
      if States (Button) /= Prev_States (Button) then
        for Sub of Subscribers loop
          if Sub /= null then
            Sub.all (Button, States (Button));
          end if; -- Subscriber_Exists
        end loop; -- Subscribers
      end if;     -- State /= Prev_State
    end loop;     -- Button
  end Handler;

  -----------
  -- State --
  -----------

  function State (Button : Button_Type) return State_Type is
  begin
    return States (Button);
  end State;

  -------------------
  -- Is_Subscribed --
  -------------------

  function Is_Subscribed (Callback : not null Callback_Type) return Boolean is
    Callback_Exists : Boolean := False;
  begin
    for Subs of Subscribers loop
      Callback_Exists := Subs = Callback;
      exit when Callback_Exists;
    end loop;
    return Callback_Exists;
  end Is_Subscribed;

  ---------------
  -- Subscribe --
  ---------------

  function Subscribe (Callback : not null Callback_Type) return Boolean is
    Is_Success : Boolean := False;
  begin
    for Subs of Subscribers loop
      if Subs = null then
        Subs := Callback;
        Is_Success := True;
      end if;
      exit when Is_Success;
    end loop;
    return Is_Success;
  end Subscribe;

  -----------------
  -- Unsubscribe --
  -----------------

  function Unsubscribe (Callback : not null Callback_Type)
                        return Boolean is
    Is_Success : Boolean := False;
  begin
    for Subs of Subscribers loop
      if Subs = Callback then
        Subs       := null;
        Is_Success := True;
      end if;
      exit when Is_Success;
    end loop;
    return Is_Success;
  end Unsubscribe;

begin
  Initialize;
end Buttons;
