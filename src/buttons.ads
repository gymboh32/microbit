package Buttons is

  type Button_Type is (A, B);
  type State_Type is (Pressed, Released);
  
  function State (Button : Button_Type) return State_Type;
  --
  -- Returns the state of the given button
  
  type Callback_Type is access procedure 
    (Button : Button_Type;
     State  : State_Type);
  --
  -- Procedure called for every button event
  
  function Is_Subscribed (Callback : not null Callback_Type) return Boolean;
  --
  -- Return True if callback is already subscribed
  
  function Subscribe (Callback : not null Callback_Type) return Boolean
    with Pre  => not Is_Subscribed (Callback),
         Post => (if Subscribe'Result then Is_Subscribed (Callback));
  --
  -- Subscribe a callback to a button event.  Returns True if successfully
  -- subscribed, otherwise returns False.
  
  function Unsubscribe (Callback : not null Callback_Type) return Boolean
    with Pre  => not Is_Subscribed (Callback),
         Post => (if UnSubscribe'Result then Is_Subscribed (Callback));
  --
  -- Unsubscribe a callback to a button event.  Returns True if successfully
  -- unsubscribed, otherwise returns False.
  
  -- TODO: Make subscriptions per Button/State???
end Buttons;
