with Buttons; use Buttons;
with Display;
with Time;
--  with Watchdog;
with Ada.Text_IO; use Ada.Text_IO;


package body Scroll is

  Toggle  : Boolean := False;
  Prev    : Buttons.Button_Type := Buttons.Button_Type'First;
  Current : Buttons.Button_Type := Buttons.Button_Type'First;

  procedure Switch is
  begin
    Toggle := not Toggle;
  end Switch;

  procedure Print is
  begin
    Put_Line (Toggle'Img);
  end Print;

  procedure Screen is
  begin
    Display.Scroll_Text("!");
  end Screen;

  procedure Button (Button : Buttons.Button_Type;
                    State  : Buttons.State_Type) is
  begin
    Put_Line (Button'Image);
    case State is
      when Buttons.Pressed =>
        Current := Button;
      when Buttons.Released =>
        null;
    end case;

  end Button;

  -------------
  -- Execute --
  -------------

  procedure Execute is
    Is_Subscribed : Boolean := False;
  begin
    Put_Line ("A");
--      Is_Subscribed := Time.Subscribe (Print'Access);
--      Is_Subscribed := Time.Subscribe (Switch'Access);
    Is_Subscribed := Buttons.Subscribe (Button'Access);

    --      Is_Subscribed := Time.Subscribe (Screen'Access);
    loop
      --        null;
      if Current /= Prev then
        Display.Scroll_Text (Current'Img);
        Prev := Current;
      end if;
        --        if Toggle then
--          Display.Scroll_Text ("T");
--        else
--          Display.Scroll_Text ("F");
--        end if;
    end loop;

  end Execute;

end Scroll;
