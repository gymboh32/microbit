with Ada.Real_Time; use Ada.Real_Time;
with Generic_Timers;
with System.Machine_Code;
with Ada.Text_IO; use Ada.Text_IO;

package body Time is

  Clock_Ms       :          Time_Ms := 0 with Volatile;
  Tick_Period_Ms : constant Time_Ms := 1;
  Tick_Period_Us : constant Integer := Integer (Tick_Period_Ms) * 1_000;

  Subscribers : array (1 .. 10) of Callback_Type := (others => null);

  procedure Time_Action;

  package Tick_Timer is new Generic_Timers
    (One_Shot   => False,
     Timer_Name => "Tick update",
     Period     => Ada.Real_Time.Microseconds (Tick_Period_Us),
     Action     => Time_Action);
  --  This timing event will call the procedure Time_Action every
  --  Tick_Period_Ms.

  ----------------
  -- Initialize --
  ----------------

  procedure Initialize is
  begin
    Tick_Timer.Start;
  end Initialize;

  ------------------
  -- Update_Clock --
  ------------------

  procedure Update_Clock is
  begin
    Clock_Ms := Clock_Ms + 1;
  end Update_Clock;

  -----------------
  -- Time_Action --
  -----------------

  procedure Time_Action is
  begin
    Update_Clock;

    for Subs of Subscribers loop
      if Subs /= null then
        Subs.all;
      end if;
    end loop;
  end Time_Action;

  -----------------
  -- Tick_Period --
  -----------------

  function Tick_Period return Time_Ms is
  begin
    return Tick_Period_Ms;
  end Tick_Period;

  --------------
  -- Delay_Ms --
  --------------

  procedure Delay_Ms (Milliseconds : Time_Ms) is
    Delay_Period : Integer := Integer (Milliseconds);
  begin
    delay until Ada.Real_Time.Clock +
      Ada.Real_Time.Milliseconds (Delay_Period);
  end Delay_Ms;

  -------------------
  -- Is_Subscribed --
  -------------------

  function Is_Subscribed (Callback : not null Callback_Type)
                          return Boolean is
    Callback_Exists : Boolean := False;
  begin
    for Subs of Subscribers loop
      Callback_Exists := Subs = Callback;
      exit when Callback_Exists;
    end loop;
    return Callback_Exists;
  end Is_Subscribed;

  ---------------
  -- Subscribe --
  ---------------

  function Subscribe (Callback : not null Callback_Type)
                      return Boolean is
    Is_Success : Boolean := False;
  begin
    for Subs of Subscribers loop
      if Subs = null then
        Subs := Callback;
        Is_Success := True;
        exit;
      end if;
    end loop;
    return Is_Success;
  end Subscribe;

  -----------------
  -- Unsubscribe --
  -----------------

  function Unsubscribe (Callback : not null Callback_Type)
                        return Boolean is
    Is_Success : Boolean := False;
  begin
    for Subs of Subscribers loop
      if Subs = Callback then
        Subs := null;
        Is_Success := True;
        exit;
      end if;
    end loop;
    return Is_Success;
  end Unsubscribe;

begin
  Initialize;
end Time;
