with HAL; use HAL;

package Time is
  
  subtype Time_Ms is UInt64;
  --
  -- Time in Milliseconds
 
  function Tick_Period return Time_Ms;
  --
  -- Returns the tick period in Milliseconds
  
  procedure Delay_Ms (Milliseconds : Time_Ms);
  --
  -- Sets a delay for specified time
  
  type Callback_Type is access procedure;
  --
  -- Procedure called for every tick period
    
  function Is_Subscribed (Callback : not null Callback_Type) return Boolean;
  --
  -- Return True if callback is already a tick event subscriber
  
  function Subscribe (Callback : not null Callback_Type) return Boolean
    with Pre  => not Is_Subscribed (Callback),
         Post => (if Subscribe'Result then Is_Subscribed (Callback));
  --
  -- Subscribe a callback to the tick event. Returns True if successfully
  -- subscribed, otherwise returns False.
  
  function Unsubscribe (Callback : not null Callback_Type) return Boolean
    with Pre  => Is_Subscribed (Callback),
         Post => (if Unsubscribe'Result then not Is_Subscribed (Callback));
  --
  -- Unsubscribe a callback to the tick event.  Returns True if successfully
  -- unsubscribed, otherwise returns False.
  
  -- TODO: Add Refresh rate for subscriptions???
end Time;
