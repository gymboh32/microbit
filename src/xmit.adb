with Beacon;

package body Xmit is

   -------------
   -- Execute --
   -------------

   procedure Execute is
  begin
    Beacon.Initialize;
    Beacon.Send_Packet;
   end Execute;

end Xmit;
